package com.example.nataniel.lab12;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class A2 extends AppCompatActivity {
    TextView fromA1;
    TextView fromA3;
    Button submitA2Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        fromA1 = findViewById(R.id.textView1A2);

        fromA1.setText("Hello " + getIntent().getStringExtra("Name"));

        fromA3 = findViewById(R.id.textViewA3);
        submitA2Button = findViewById(R.id.submitA2Button);
        submitA2Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(A2.this, A3.class);
                startActivityForResult(intent, 2);  //get requestCode from activity 3
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TextView textView1 = findViewById(R.id.textView2A2);
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 2) {
            String message = data.getStringExtra("Message");
            textView1.setText("From A3: " + message);
        }
    }
}
