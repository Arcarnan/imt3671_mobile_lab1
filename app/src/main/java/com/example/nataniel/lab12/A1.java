package com.example.nataniel.lab12;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class A1 extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);

        final Button submitA1Button = findViewById(R.id.submitA1);
        final EditText textFromA1 = findViewById(R.id.textViewOfA1);

        submitA1Button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(A1.this, A2.class);
                intent.putExtra("Name", textFromA1.getText().toString());
                startActivity(intent);
            }
        });
        // For dropdown menu:
        final Spinner spinner = findViewById(R.id.dropdownA1);

        //  list for dropdown choices
        List<String> categories = new ArrayList<>();
        categories.add("Test1");
        categories.add("Test2");
        categories.add("temp123");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final int L1 = prefs.getInt("L1", -1);
        if (L1 != -1) {
            spinner.setSelection(L1);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        final SharedPreferences.Editor editor = prefs.edit();
        final Spinner spinner = findViewById(R.id.dropdownA1);
        final int L1 = spinner.getSelectedItemPosition();
        editor.putInt("L1", L1);

        editor.apply();
    }
}